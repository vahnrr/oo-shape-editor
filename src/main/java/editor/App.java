package editor;

import editor.gui.GuiLibrary;
import editor.gui.Window;
import editor.gui.javafx.JavaFxGui;
import editor.gui.layout.HeaderBar;
import editor.gui.layout.ToolBar;
import editor.gui.layout.Whiteboard;
import editor.shape.javafx.*;
import editor.util.Colour;
import editor.util.Position;

public class App {
    public static void main(String[] args) {
        CompositeShape cs = new CompositeShape();
        Shape c1 = new Circle(new Position(10, 10), Colour.RED, 10),
              c2 = new Circle(new Position(100, 40), Colour.GREEN, 30),
              rp1 = new RegularPolygon(new Position(20, 30), Colour.BLUE, 3, 200.0),
              rp2 = new RegularPolygon(new Position(90, 100), Colour.RED, 9, 70.0),
              r1 = new Rectangle(new Position(20, 50), new Colour(138, 180, 248), 50, 20);
        cs.add(c1);
        cs.add(r1);
        cs.add(c2);
        cs.add(rp2);
        Shape[] builtinShapes = { rp1, cs, r1, rp2 };

        GuiLibrary guiLib = new JavaFxGui();
        HeaderBar headerBar = guiLib.createHeaderBar();
        ToolBar toolBar = guiLib.createToolBar(builtinShapes);
        Whiteboard whiteboard = guiLib.createWhiteboard();

        Window window = guiLib.createWindow("Shape editor", headerBar, toolBar, whiteboard);
        guiLib.render();
    }
}
