package editor.shape;

import editor.util.Colour;
import editor.util.Position;

public abstract class GenericShape implements Shape {
    private static final Colour DEFAULT_COLOUR = new Colour(255, 0, 0);
    private static final double DEFAULT_ROTATION = 0.0;
    private static final Colour DEFAULT_STROKE_COLOUR = Colour.BLACK;
    private static final int DEFAULT_STROKE_WIDTH = 1;

    protected Position position, rotationCentre;
    protected double rotation;
    protected Colour colour, strokeColour;
    protected int strokeWidth;

    public GenericShape(Position position, Colour colour) {
        this.position = position;
        this.rotationCentre = position;
        this.colour = colour;
        rotation = DEFAULT_ROTATION;
        strokeColour = DEFAULT_STROKE_COLOUR;
        strokeWidth = DEFAULT_STROKE_WIDTH;
    }

    public GenericShape(Position position) {
        this(position, DEFAULT_COLOUR);
    }

    @Override
    public Position getPosition() {
        return position;
    }

    public double getRotation() {
        return rotation;
    }

    public Colour getColour() {
        return colour;
    }

    public Colour getStrokeColour() {
        return strokeColour;
    }

    public int getStrokeWidth() {
        return strokeWidth;
    }

    @Override
    public void resize() {}

    @Override
    public void rotate() {}

    @Override
    public void translate(double xAmount, double yAmount) {
        position.setX(position.getX() + xAmount);
        position.setY(position.getY() + yAmount);
    }

    @Override
    public void move(double x, double y) {
        position.setX(x);
        position.setY(y);
    }

    @Override
    public String toSvg() {
        return "<svg>" + toSvgShape() + "</svg>";
    }

    @Override
    public GenericShape clone() {
        GenericShape clone;
        try {
            clone = (GenericShape) super.clone();
            clone.position = clone.position.clone();
            clone.rotationCentre = clone.rotationCentre.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException("Could not clone CompositeShape", e);
        }
        return clone;
    }
}
