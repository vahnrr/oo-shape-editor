package editor.shape;

import editor.util.Colour;
import editor.util.Position;

import javafx.scene.Node;
import javafx.scene.shape.Shape;

public class Rectangle extends GenericShape {
    private static final int DEFAULT_CORNER_RADIUS = 0;

    // translation?
    private int width, height, cornerRadius;

    public Rectangle(Position position, Colour colour, int width, int height) {
        super(position, colour);
        this.width = width;
        this.height = height;
        cornerRadius = DEFAULT_CORNER_RADIUS;
    }

    @Override
    public String toSvgShape() {
        return "<rect width='" + width + "'"
            + " height='" + height + "'"
            + " fill='" + colour + "'"
            + " stroke='" + strokeColour + "'"
            + " stroke-width='" + strokeWidth + "' />";
    }

    @Override
    public String toSvgPath() {
        return "M " + position.getX() + " " + position.getY()
            + " h " + width
            + " v " + height
            + " h -" + width
            + " Z";
    }
}
