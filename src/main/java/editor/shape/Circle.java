package editor.shape;

import editor.util.Colour;
import editor.util.Position;

public class Circle extends GenericShape {
    private int radius;

    public Circle(Position position, Colour colour, int radius) {
        super(position, colour);
        this.radius = radius;
    }

    @Override
    public void resize() {}

    @Override
    public void rotate() {}

    @Override
    public String toSvgShape() {
        return "<circle cx='" + position.getX() + "'"
            + " cy='" + position.getY() + "'"
            + " r='" + radius + "'"
            + " fill='" + colour + "'"
            + " stroke='" + strokeColour + "'"
            + " stroke-width='" + strokeWidth + "' />";
    }

    @Override
    public String toSvgPath() {
        return "M " + position.getX() + " " + position.getY()
            + " m -" + radius + " 0"
            + " a " + radius + " " + radius + " 0 1 1 " + (radius*2) + " 0"
            + " a " + radius + " " + radius + " 0 1 1 -" + (radius*2) + " 0"
            + " Z";
    }
}
