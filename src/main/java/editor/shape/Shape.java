package editor.shape;

import editor.util.Colour;
import editor.util.Position;

public interface Shape extends Cloneable {
    public Position getPosition();

    public void resize();

    public void rotate();

    public void translate(double xAmount, double yAmount);

    public void move(double x, double y);

    public String toSvg();

    public String toSvgShape();

    public String toSvgPath();

    public Shape clone();
}
