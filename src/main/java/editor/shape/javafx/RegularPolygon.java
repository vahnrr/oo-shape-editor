package editor.shape.javafx;

import editor.util.Colour;
import editor.util.Position;

import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.SVGPath;

public class RegularPolygon extends editor.shape.RegularPolygon implements Shape {
    public RegularPolygon(Position position, Colour colour, int sidesCount, double sideLength) {
        super(position, colour, sidesCount, sideLength);
    }

    @Override
    public Node getGuiElement() {
        SVGPath node = new SVGPath();
        node.setContent(toSvgPath());
        node.setFill(Color.web(colour.toString()));
        node.setStroke(Color.web(strokeColour.toString()));
        node.setStrokeWidth(strokeWidth);
        return node;
    }

    @Override
    public void applyStyles() {
    }
}
