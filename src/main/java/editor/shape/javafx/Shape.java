package editor.shape.javafx;

import editor.gui.javafx.GuiElement;

import javafx.scene.Node;

public interface Shape extends editor.shape.Shape, GuiElement {
}
