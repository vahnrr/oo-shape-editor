package editor.shape.javafx;

import java.util.HashSet;
import javafx.scene.Node;
import javafx.scene.Group;

public class CompositeShape extends editor.shape.CompositeShape implements Shape {
    public CompositeShape() {
        super();
    }

    @Override
    public Node getGuiElement() {
        Group group = new Group();
        for (editor.shape.Shape child : children) {
            group.getChildren().add(((Shape) child).getGuiElement());
        }
        return group;
    }

    @Override
    public void applyStyles() {
    }
}
