package editor.shape.javafx;

import editor.util.Colour;
import editor.util.Position;

import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.SVGPath;

public class Circle extends editor.shape.Circle implements Shape {
    public Circle(Position position, Colour colour, int radius) {
        super(position, colour, radius);
    }

    @Override
    public Node getGuiElement() {
        SVGPath node = new SVGPath();
        node.setContent(toSvgPath());
        node.setFill(Color.web(colour.toString()));
        node.setStroke(Color.web(strokeColour.toString()));
        node.setStrokeWidth(strokeWidth);
        return node;
    }

    @Override
    public void applyStyles() {
    }
}
