package editor.shape;

import editor.util.Position;

import java.util.HashSet;
import javafx.scene.Node;
import javafx.scene.Group;
import javafx.scene.shape.Rectangle;

public class CompositeShape implements Shape {
    private Position position, rotationCentre;
    private double rotation;
    protected HashSet<Shape> children;

    public CompositeShape() {
        children = new HashSet<Shape>();
    }

    public CompositeShape(HashSet<Shape> shapes) {
        children = (HashSet<Shape>) shapes.clone();
    }

    @Override
    public Position getPosition() {
        return position;
    }

    @Override
    public void resize() {}

    @Override
    public void rotate() {}

    @Override
    public void translate(double xAmount, double yAmount) {
        position.setX(position.getX() + xAmount);
        position.setY(position.getY() + yAmount);
        for (Shape child : children) {
            child.translate(xAmount, yAmount);
        }
    }

    @Override
    public void move(double x, double y) {
        position.setX(x);
        position.setY(y);
        for (Shape child : children) {
            child.move(x, y);
        }
    }

    @Override
    public String toSvg() {
        return "<svg>" + toSvgShape() + "</svg>";
    }

    @Override
    public String toSvgShape() {
        String svg = "\n";
        for (Shape child : children) {
            svg += child.toSvgShape() + "\n";
        }
        return svg;
    }

    @Override
    public String toSvgPath() {
        String path = "";
        for (Shape child : children) {
            path += child.toSvgPath() + "\n";
        }
        return path;
    }

    @Override
    public CompositeShape clone() {
        CompositeShape clone;
        try {
            clone = (CompositeShape) super.clone();
            for (Shape child : clone.children) {
                child = child.clone();
            }
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException("Could not clone CompositeShape", e);
        }
        return clone;
    }

    public void add(Shape shape) {
        children.add(shape);
    }

    public void remove(Shape shape) {
        children.remove(shape);
    }
}
