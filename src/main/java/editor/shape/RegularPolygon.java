package editor.shape;

import editor.util.Colour;
import editor.util.Position;

import javafx.scene.Node;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Shape;

public class RegularPolygon extends GenericShape {
    // translation?
    private int sidesCount;
    private double sideLength;
    private double[][] points;

    public RegularPolygon(Position position, Colour colour, int sidesCount, double sideLength) {
        super(position, colour);
        this.sidesCount = sidesCount;
        this.sideLength = sideLength;
        computePoints();
    }

    @Override
    public String toSvgShape() {
        String svg = "<polygon points='";
        for (int i = 0; i < sidesCount; i++) {
            svg += points[i][0] + "," + points[i][1] + " ";
        }
        return svg + "' />";
    }

    @Override
    public String toSvgPath() {
        String path = "M " + points[0][0] + " " + points[0][1];
        for (int i = 0; i < sidesCount; i++) {
            path += " L " + points[i][0] + " " + points[i][1];
        }
        return path + " Z";
    }

    private void computePoints() {
        points = new double[sidesCount][2];
        for (int i = 0; i < sidesCount; i++) {
            points[i][0] = position.getX() + (sideLength * Math.cos(2.0 * Math.PI * i / sidesCount));
            points[i][1] = position.getY() + (sideLength * Math.sin(2.0 * Math.PI * i / sidesCount));
        }
    }
}
