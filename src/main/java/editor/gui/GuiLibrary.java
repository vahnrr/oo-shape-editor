package editor.gui;

import editor.gui.layout.HeaderBar;
import editor.gui.layout.ToolBar;
import editor.gui.layout.Whiteboard;
import editor.shape.Shape;

public interface GuiLibrary {
    public HeaderBar createHeaderBar();

    public ToolBar createToolBar(Shape[] builtinShapes);

    public Whiteboard createWhiteboard();

    public Window createWindow(String title, HeaderBar headerBar, ToolBar toolBar, Whiteboard whiteboard);

    public void render();
}
