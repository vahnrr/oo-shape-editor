package editor.gui.control;

public interface SaveButton extends ClickButton {
    public static final String FILENAME = "save";
}
