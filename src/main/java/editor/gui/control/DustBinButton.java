package editor.gui.control;

import editor.util.Colour;

public interface DustBinButton extends DragDroppedButton {
    public static final Colour HOVER_BACKGROUND_COLOUR = new Colour(191, 97, 106);
    public static final String FILENAME = "dust-bin";
}
