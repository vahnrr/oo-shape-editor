package editor.gui.control;

public interface LoadButton extends ClickButton {
    public static final String FILENAME = "load";
}
