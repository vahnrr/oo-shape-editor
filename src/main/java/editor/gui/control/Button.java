package editor.gui.control;

import editor.util.Colour;

public interface Button {
    public static final Colour BACKGROUND_COLOUR = new Colour(85, 85, 85);
}
