package editor.gui.control;

import editor.util.Colour;

public interface ClickButton extends ImageButton {
    public static final Colour HOVER_BACKGROUND_COLOUR = new Colour(102, 102, 102);
}
