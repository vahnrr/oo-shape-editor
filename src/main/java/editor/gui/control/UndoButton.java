package editor.gui.control;

public interface UndoButton extends ClickButton {
    public static final String FILENAME = "undo";
}
