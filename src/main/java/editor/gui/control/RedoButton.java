package editor.gui.control;

public interface RedoButton extends ClickButton {
    public static final String FILENAME = "redo";
}
