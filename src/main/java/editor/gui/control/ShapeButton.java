package editor.gui.control;

import editor.shape.Shape;

public interface ShapeButton extends DragDetectedButton {
    public static final double SHAPE_SIZE = 48.0,
                               SIZE = SHAPE_SIZE + 16.0;
}
