package editor.gui.control;

import editor.util.Colour;

public interface AddCustomShapeButton extends DragDroppedButton {
    public static final Colour HOVER_BACKGROUND_COLOUR = new Colour(166, 190, 140);
    public static final String FILENAME = "plus";
}
