package editor.gui.control;

import editor.util.Colour;

public interface DragDroppedButton extends ImageButton {
    public void defineOnDragEntered();

    public void defineOnDragExited();

    public void defineOnDragOver();

    public void defineOnDragDropped();
}
