package editor.gui.control;

public interface ImageButton extends Button {
    public static final double SIZE = 24.0;
    public static final String IMAGE_EXTENSION = ".png",
                               IMAGE_FOLDER_PATH = "src/main/resources/images/";
}
