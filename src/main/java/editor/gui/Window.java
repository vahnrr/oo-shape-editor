package editor.gui;

import editor.gui.layout.HeaderBar;
import editor.gui.layout.ToolBar;
import editor.gui.layout.Whiteboard;

public class Window {
    private String title;
    private HeaderBar headerBar;
    private ToolBar toolBar;
    private Whiteboard whiteboard;

    public Window(String title, HeaderBar headerBar, ToolBar toolBar, Whiteboard whiteboard) {
        this.title = title;
        this.headerBar = headerBar;
        this.toolBar = toolBar;
        this.whiteboard = whiteboard;
    }

    public String getTitle() {
        return title;
    }
}
