package editor.gui.layout;

import editor.shape.CompositeShape;
import editor.shape.Shape;
import editor.util.Colour;

import java.util.HashSet;

public class Whiteboard implements Cloneable {
    protected static final Colour BACKGROUND_COLOUR = new Colour(221, 221, 221);

    private HashSet<Shape> shapes;

    public Whiteboard() {
        shapes = new HashSet<Shape>();
    }

    public void add(Shape shape) {
        shapes.add(shape.clone());
    }

    public void remove(Shape shape) {
        shapes.remove(shape);
    }

    public CompositeShape toCompositeShape() {
        return new CompositeShape(shapes);
    }

    public Whiteboard clone() {
        try {
            return (Whiteboard) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException("Could not clone Whiteboard", e);
        }
    }
}
