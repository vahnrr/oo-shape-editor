package editor.gui.layout;

import editor.gui.control.Button;
import editor.util.Colour;

import java.util.HashSet;

public class ToolBar {
    protected static final Colour BACKGROUND_COLOUR = new Colour(68, 68, 68),
                                  SEPARATOR_COLOUR = new Colour(187, 187, 187);

    protected HashSet<Button> builtinShapesElements,
                            customShapesElements,
                            toolsElements;

    public ToolBar() {
        builtinShapesElements = new HashSet<>();
        customShapesElements = new HashSet<>();
        toolsElements = new HashSet<>();
    }

    public void addCustomShape(Button customShapeButton) {
        customShapesElements.add(customShapeButton);
    }

    public void removeCustomShape(Button customShapeButton) {
        customShapesElements.remove(customShapeButton);
    }
}
