package editor.gui.layout;

import editor.gui.control.Button;
import editor.util.Colour;

import java.util.HashSet;

public class HeaderBar {
    protected static final Colour BACKGROUND_COLOUR = new Colour(51, 51, 51);

    protected HashSet<Button> buttons;

    public HeaderBar() {
        buttons = new HashSet<>();
    }
}
