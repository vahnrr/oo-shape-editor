package editor.gui.javafx;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class JavaFxApp extends Application {
    private static String appTitle;
    private static Scene appScene;

    public static void setTitle(String title) {
        appTitle = title;
    }

    public static void setScene(Scene scene) {
        appScene = scene;
    }

    public static void render() {
        launch(new String[0]);
    }

    @Override
    public void start(Stage stage) {
        stage.setTitle(appTitle);
        stage.setScene(appScene);
        stage.show();
    }
}
