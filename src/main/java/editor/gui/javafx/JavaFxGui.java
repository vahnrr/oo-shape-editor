package editor.gui.javafx;

import editor.gui.GuiLibrary;
import editor.gui.layout.HeaderBar;
import editor.gui.layout.ToolBar;
import editor.gui.layout.Whiteboard;
import editor.shape.Shape;

import javafx.application.Platform;

public class JavaFxGui implements GuiLibrary {
    private Window window;

    public JavaFxGui() {
        // Initialises the JavaFX toolkit
        Platform.startup(() -> {});
    }

    @Override
    public HeaderBar createHeaderBar() {
        return new editor.gui.javafx.layout.HeaderBar();
    }

    @Override
    public ToolBar createToolBar(Shape[] builtinShapes) {
        return new editor.gui.javafx.layout.ToolBar((editor.shape.javafx.Shape[]) builtinShapes);
    }

    @Override
    public Whiteboard createWhiteboard() {
        return new editor.gui.javafx.layout.Whiteboard();
    }

    @Override
    public Window createWindow(String title, HeaderBar headerBar, ToolBar toolBar, Whiteboard whiteboard) {
        window = new editor.gui.javafx.Window(title,
                                              (editor.gui.javafx.layout.HeaderBar) headerBar,
                                              (editor.gui.javafx.layout.ToolBar) toolBar,
                                              (editor.gui.javafx.layout.Whiteboard) whiteboard);
        return window;
    }

    @Override
    public void render() {
        JavaFxApp.setTitle(window.getTitle());
        JavaFxApp.setScene(window.getScene());
        JavaFxApp.render();
    }
}
