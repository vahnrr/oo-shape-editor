package editor.gui.javafx.layout;

import editor.gui.javafx.GuiElement;
import editor.gui.javafx.control.Button;
import editor.gui.javafx.control.DustBinButton;
import editor.gui.javafx.control.AddCustomShapeButton;
import editor.gui.javafx.control.ShapeButton;
import editor.shape.javafx.Shape;

import javafx.geometry.Pos;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

public class ToolBar extends editor.gui.layout.ToolBar implements GuiElement {
    private static final double SPACING = 12;

    private VBox container,
                 builtinShapesContainer,
                 customShapesContainer,
                 toolsContainer;

    public ToolBar(Shape[] builtinShapes) {
        super();

        builtinShapesContainer = new VBox(SPACING);
        builtinShapesContainer.setAlignment(Pos.TOP_CENTER);
        for (Shape shape : builtinShapes) {
            Button shapeButton = new ShapeButton(shape);
            builtinShapesElements.add(shapeButton);
            builtinShapesContainer.getChildren().add(shapeButton.getGuiElement());
        }

        Button addCustomShapeButton = new AddCustomShapeButton();
        customShapesElements.add(addCustomShapeButton);
        customShapesContainer = new VBox(SPACING, addCustomShapeButton.getGuiElement());
        customShapesContainer.setAlignment(Pos.TOP_CENTER);
        VBox.setVgrow(customShapesContainer, Priority.ALWAYS);

        Button dustBinButton = new DustBinButton();
        toolsElements.add(dustBinButton);
        toolsContainer = new VBox(SPACING, dustBinButton.getGuiElement());
        toolsContainer.setAlignment(Pos.TOP_CENTER);

        container = new VBox(SPACING,
                             builtinShapesContainer,
                             customShapesContainer,
                             toolsContainer);
        applyStyles();
    }

    @Override
    public VBox getGuiElement() {
        return container;
    }

    @Override
    public void applyStyles() {
        container.setStyle("-fx-background-color: " + BACKGROUND_COLOUR + ";"
                           + "-fx-padding: " + SPACING + "px;"
                           + "-fx-view-order: -1.0;");
        builtinShapesContainer.setStyle("-fx-padding: 0 0 " + SPACING + "px 0;"
                                        + "-fx-border-width: 0 0 2px 0;"
                                        + "-fx-border-color: " + SEPARATOR_COLOUR + ";");
    }
}
