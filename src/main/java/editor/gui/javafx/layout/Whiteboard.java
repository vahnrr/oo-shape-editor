package editor.gui.javafx.layout;

import editor.gui.javafx.GuiElement;
import editor.shape.javafx.Circle;
import editor.shape.javafx.CompositeShape;
import editor.shape.javafx.Rectangle;
import editor.shape.javafx.RegularPolygon;
import editor.shape.javafx.Shape;
import editor.util.Colour;
import editor.util.Position;

import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.DragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.Pane;

public class Whiteboard extends editor.gui.layout.Whiteboard implements GuiElement {
    private Pane canvas;

    public Whiteboard() {
        super();
        canvas = new Pane();

        // Shapes on the Whiteboard when started
        CompositeShape cs = new CompositeShape();
        Shape c1 = new Circle(new Position(10, 20), Colour.BLUE, 50),
              c2 = new Circle(new Position(100, 400), Colour.GREEN, 30),
              r1 = new Rectangle(new Position(200, 150), new Colour(56, 89, 102), 80, 20),
              rp1 = new RegularPolygon(new Position(400, 70), Colour.BLUE, 3, 60.0);
        cs.add(c2);
        cs.add(r1);
        add(c1);
        add(cs);
        add(rp1);

        applyStyles();
        canvas.setOnDragOver(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                if (event.getGestureSource() != canvas) {
                    event.acceptTransferModes(TransferMode.COPY);
                }
                event.consume();
            }
        });
        canvas.setOnDragDropped(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                System.out.println("[JavaFX] :: Whiteboard drag dropped!");
                Dragboard clipboard = event.getDragboard();
                if (clipboard.hasString()) {
                    System.out.println(clipboard.getString());
                }
                event.setDropCompleted(true);
                event.consume();
            }
        });
    }

    @Override
    public Node getGuiElement() {
        return canvas;
    }

    @Override
    public void applyStyles() {
        canvas.setStyle("-fx-background-color: " + BACKGROUND_COLOUR + ";"
                        + "-fx-cursor: hand;");
    }

    public void add(Shape shape) {
        super.add(shape);
        canvas.getChildren().add(shape.getGuiElement());
        setOnShapeMousePressed(shape, canvas.getChildren().size() - 1);
    }

    public void remove(Shape shape) {
        super.remove(shape);
        canvas.getChildren().remove(shape.getGuiElement());
    }

    private void setOnShapeMousePressed(Shape shape, int id) {
        Node guiElement = canvas.getChildren().get(id);
        guiElement.setOnDragDetected(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent event) {
                System.out.println("[JavaFX] :: Drag detected on this Shape!");
                System.out.println("==> Shape: " + shape.getPosition().getX() + "," + shape.getPosition().getY());
                System.out.println("==> Event: " + event.getSceneX() + "," + event.getSceneY());
                // shape.move(event.getSceneX(), event.getSceneY());
                // guiElement.setTranslateX(event.getSceneX());
                // guiElement.setTranslateY(event.getSceneY());
                Dragboard clipboard = guiElement.startDragAndDrop(TransferMode.MOVE);
                ClipboardContent content = new ClipboardContent();
                content.putString(shape.toSvgShape());
                clipboard.setContent(content);
                event.consume();
            }
        });
        guiElement.setOnDragDone(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                // shape.move(event.getSceneX(), event.getSceneY());
                // guiElement.setTranslateX(event.getSceneX());
                // guiElement.setTranslateY(event.getSceneY());
                if (event.getTransferMode() == TransferMode.MOVE) {
                    System.out.println("[JavaFX] :: Drag done on this Shape!");
                    System.out.println("==> Shape: " + shape.getPosition().getX() + "," + shape.getPosition().getY());
                    System.out.println("==> Event: " + event.getSceneX() + "," + event.getSceneY());
                }
                event.consume();
            }
        });
    }
}
