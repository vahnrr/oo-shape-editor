package editor.gui.javafx.layout;

import editor.gui.javafx.GuiElement;
import editor.gui.javafx.control.Button;
import editor.gui.javafx.control.LoadButton;
import editor.gui.javafx.control.RedoButton;
import editor.gui.javafx.control.SaveButton;
import editor.gui.javafx.control.UndoButton;

import javafx.scene.layout.HBox;

public class HeaderBar extends editor.gui.layout.HeaderBar implements GuiElement {
    private static final double SPACING = 12;

    private HBox container;

    public HeaderBar() {
        super();
        Button saveButton = new SaveButton(),
               loadButton = new LoadButton(),
               undoButton = new UndoButton(),
               redoButton = new RedoButton();
        buttons.add(saveButton);
        buttons.add(loadButton);
        buttons.add(undoButton);
        buttons.add(redoButton);
        container = new HBox(SPACING,
                             saveButton.getGuiElement(),
                             loadButton.getGuiElement(),
                             undoButton.getGuiElement(),
                             redoButton.getGuiElement());
        applyStyles();
    }

    @Override
    public HBox getGuiElement() {
        return container;
    }

    @Override
    public void applyStyles() {
        container.setStyle("-fx-background-color: " + BACKGROUND_COLOUR + ";"
                           + "-fx-padding: " + SPACING + "px;"
                           + "-fx-view-order: -2.0;");
    }
}
