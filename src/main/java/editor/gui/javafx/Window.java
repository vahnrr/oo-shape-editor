package editor.gui.javafx;

import editor.gui.javafx.layout.HeaderBar;
import editor.gui.javafx.layout.ToolBar;
import editor.gui.javafx.layout.Whiteboard;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

public class Window extends editor.gui.Window {
    private final static double DEFAULT_WIDTH = 900.0,
                                DEFAULT_HEIGHT = 600.0;

    private Scene scene;
    private Parent root;
    private HBox subContainer;

    public Window(String title, HeaderBar headerBar, ToolBar toolBar, Whiteboard whiteboard) {
        super(title, headerBar, toolBar, whiteboard);
        try {
            subContainer = new HBox(toolBar.getGuiElement(), whiteboard.getGuiElement());
            root = new VBox(headerBar.getGuiElement(), subContainer);
            scene = new Scene(root, DEFAULT_WIDTH, DEFAULT_HEIGHT);
            setAutoGrowth((Pane) whiteboard.getGuiElement());
        } catch (Exception e) {
            throw new RuntimeException("[JavaFX] Could not create Window", e);
        }
    }

    public Scene getScene() {
        return scene;
    }

    private void setAutoGrowth(Pane whiteboard) {
        subContainer.prefHeightProperty().bind(scene.heightProperty());
        whiteboard.prefWidthProperty().bind(subContainer.widthProperty());
        whiteboard.prefHeightProperty().bind(subContainer.heightProperty());
    }
}
