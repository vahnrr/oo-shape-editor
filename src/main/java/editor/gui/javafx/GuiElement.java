package editor.gui.javafx;

import javafx.scene.Node;

public interface GuiElement {
    public Node getGuiElement();

    public void applyStyles();
}
