package editor.gui.javafx.control;

import javafx.event.EventHandler;
import javafx.scene.input.DragEvent;

public class DustBinButton extends DragDroppedButton
                           implements editor.gui.control.DustBinButton {
    private static final EventHandler<DragEvent> ON_DRAG_DROPPED_HANDLER = new EventHandler<DragEvent>() {
        public void handle(DragEvent event) {
            System.out.println("[JavaFX] :: DustBinButton drag dropped!");
            event.setDropCompleted(true);
            event.consume();
        }
    };

    public DustBinButton() {
        super(FILENAME, HOVER_BACKGROUND_COLOUR, ON_DRAG_DROPPED_HANDLER);
    }
}
