package editor.gui.javafx.control;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

public class SaveButton extends ClickButton
                        implements editor.gui.control.SaveButton {
    public SaveButton() {
        super(FILENAME);
        getGuiElement().setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent event) {
                System.out.println("[JavaFX] :: SaveButton clicked!");
            }
        });
    }
}
