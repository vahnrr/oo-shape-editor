package editor.gui.javafx.control;

import editor.util.Colour;

import java.io.FileInputStream;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;

public abstract class ImageButton implements Button, editor.gui.control.ImageButton {
    private javafx.scene.control.Button button;

    public ImageButton(String filename) {
        try {
            Image image = new Image(new FileInputStream(IMAGE_FOLDER_PATH
                                                        + filename
                                                        + IMAGE_EXTENSION));
            ImageView imageView = new ImageView(image);
            imageView.setFitHeight(SIZE);
            imageView.setFitWidth(SIZE);
            button = new javafx.scene.control.Button("", imageView);
            applyStyles();
        } catch(Exception e) {
            throw new RuntimeException("[JavaFX] Could not create ClickButton", e);
        }
    }

    @Override
    public Node getGuiElement() {
        return button;
    }

    @Override
    public void applyStyles() {
        button.setStyle("-fx-background-color: " + BACKGROUND_COLOUR + ";"
                        + "-fx-background-radius: 0%;"
                        + "-fx-cursor: hand;");
    }
}
