package editor.gui.javafx.control;

import editor.gui.javafx.GuiElement;

public interface Button extends editor.gui.control.Button, GuiElement {
}
