package editor.gui.javafx.control;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

public class LoadButton extends ClickButton
                        implements editor.gui.control.LoadButton {
    public LoadButton() {
        super(FILENAME);
        getGuiElement().setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent event) {
                System.out.println("[JavaFX] :: LoadButton clicked!");
            }
        });
    }
}
