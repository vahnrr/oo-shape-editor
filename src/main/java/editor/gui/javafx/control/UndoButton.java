package editor.gui.javafx.control;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

public class UndoButton extends ClickButton
                        implements editor.gui.control.UndoButton {
    public UndoButton() {
        super(FILENAME);
        getGuiElement().setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent event) {
                System.out.println("[JavaFX] :: UndoButton clicked!");
            }
        });
    }
}
