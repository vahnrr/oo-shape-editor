package editor.gui.javafx.control;

import editor.util.Colour;

import javafx.scene.Node;

public abstract class ClickButton extends ImageButton implements editor.gui.control.ClickButton {
    public ClickButton(String filename) {
        super(filename);
        String baseStyle = "-fx-background-radius: 0%;-fx-cursor: hand;";
        Node button = getGuiElement();
        button.setOnMouseEntered(e -> button.setStyle(baseStyle
                                                      + "-fx-background-color: "
                                                      + HOVER_BACKGROUND_COLOUR + ";"));
        button.setOnMouseExited(e -> button.setStyle(baseStyle
                                                     + "-fx-background-color: "
                                                     + BACKGROUND_COLOUR + ";"));
    }
}
