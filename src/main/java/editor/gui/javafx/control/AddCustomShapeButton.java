package editor.gui.javafx.control;

import javafx.event.EventHandler;
import javafx.scene.input.DragEvent;

public class AddCustomShapeButton extends DragDroppedButton
                                  implements editor.gui.control.AddCustomShapeButton {
    private static final EventHandler<DragEvent> ON_DRAG_DROPPED_HANDLER = new EventHandler<DragEvent>() {
        public void handle(DragEvent event) {
            System.out.println("[JavaFX] :: AddCustomShapeButton drag dropped!");
            event.setDropCompleted(true);
            event.consume();
        }
    };

    public AddCustomShapeButton() {
        super(FILENAME, HOVER_BACKGROUND_COLOUR, ON_DRAG_DROPPED_HANDLER);
    }
}
