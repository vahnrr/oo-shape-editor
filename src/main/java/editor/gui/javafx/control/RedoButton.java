package editor.gui.javafx.control;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

public class RedoButton extends ClickButton
                        implements editor.gui.control.RedoButton {
    public RedoButton() {
        super(FILENAME);
        getGuiElement().setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent event) {
                System.out.println("[JavaFX] :: RedoButton clicked!");
            }
        });
    }
}
