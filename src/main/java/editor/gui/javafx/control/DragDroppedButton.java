package editor.gui.javafx.control;

import editor.util.Colour;

import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.DragEvent;
import javafx.scene.input.TransferMode;

public abstract class DragDroppedButton extends ImageButton implements editor.gui.control.DragDroppedButton {
    private Colour hoverBackgroundColour;
    private EventHandler<DragEvent> onDragDroppedHandler;

    public DragDroppedButton(String filename,
                             Colour hoverBackgroundColour,
                             EventHandler<DragEvent> onDragDroppedHandler) {
        super(filename);
        this.hoverBackgroundColour = hoverBackgroundColour;
        this.onDragDroppedHandler = onDragDroppedHandler;
        defineOnDragEntered();
        defineOnDragExited();
        defineOnDragOver();
        defineOnDragDropped();
    }

    @Override
    public void defineOnDragEntered() {
        getGuiElement().setOnDragEntered(e -> getGuiElement().setStyle("-fx-background-color: "
                                                                       + hoverBackgroundColour + ";"));
    }

    @Override
    public void defineOnDragExited() {
        getGuiElement().setOnDragExited(e -> getGuiElement().setStyle("-fx-background-color: "
                                                                      + BACKGROUND_COLOUR + ";"));
    }

    @Override
    public void defineOnDragOver() {
        getGuiElement().setOnDragOver(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                event.acceptTransferModes(TransferMode.COPY);
                event.consume();
            }
        });
    }

    @Override
    public void defineOnDragDropped() {
        getGuiElement().setOnDragDropped(onDragDroppedHandler);
    }
}
