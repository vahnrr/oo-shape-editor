package editor.gui.javafx.control;

import editor.shape.javafx.Shape;

import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.DragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;

public class ShapeButton extends DragDetectedButton
                         implements editor.gui.control.ShapeButton {
    private Shape shape;

    public ShapeButton(Shape shape) {
        super(SIZE);
        this.shape = shape;
        Group group = new Group();
        group.getChildren().add(shape.getGuiElement());
        group = scaleGroup(group);
        button = new Button("", group);
        super.applyStyles();
        button.setOnDragDetected(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent event) {
                System.out.println("[JavaFX] :: ShapeButton drag detected!");
                Dragboard clipboard = button.startDragAndDrop(TransferMode.COPY);
                ClipboardContent content = new ClipboardContent();
                content.putString(shape.toSvgShape());
                clipboard.setContent(content);
                event.consume();
            }
        });
        button.setOnDragDone(new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {
                if (event.getTransferMode() == TransferMode.COPY) {
                    event.getDragboard().clear();
                }
                event.consume();
            }
        });
    }

    private Group scaleGroup(Group group) {
        double scale = SHAPE_SIZE / Math.max(group.prefWidth(SHAPE_SIZE),
                                             group.prefHeight(SHAPE_SIZE));
        group.setScaleX(scale);
        group.setScaleY(scale);
        return group;
    }
}
