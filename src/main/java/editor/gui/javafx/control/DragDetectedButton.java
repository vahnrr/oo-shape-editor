package editor.gui.javafx.control;

import javafx.scene.Node;

public abstract class DragDetectedButton implements Button, editor.gui.control.DragDetectedButton {
    protected javafx.scene.control.Button button;
    private double size;

    public DragDetectedButton(double size) {
        this.size = size;
    }

    @Override
    public Node getGuiElement() {
        return button;
    }

    @Override
    public void applyStyles() {
        button.setMinHeight(size);
        button.setPrefHeight(size);
        button.setMaxHeight(size);
        button.setMinWidth(size);
        button.setPrefWidth(size);
        button.setMaxWidth(size);
        String baseStyle = "-fx-background-radius: 0%;";
        button.setStyle(baseStyle + "-fx-background-color: " + BACKGROUND_COLOUR + ";");
        button.setOnMouseEntered(e -> button.setStyle(baseStyle
                                                      + "-fx-cursor: hand;"
                                                      + "-fx-background-color: "
                                                      + HOVER_BACKGROUND_COLOUR + ";"));
        button.setOnMouseExited(e -> button.setStyle(baseStyle
                                                     + "-fx-cursor: move;"
                                                     + "-fx-background-color: "
                                                     + BACKGROUND_COLOUR + ";"));
    }
}
