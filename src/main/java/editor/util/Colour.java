package editor.util;

public class Colour {
    private static final int MIN_HEX_VALUE = 0;
    private static final int MAX_HEX_VALUE = 255;

    public static final Colour BLACK = new Colour(0, 0, 0);
    public static final Colour BLUE = new Colour(0, 0, 255);
    public static final Colour GREEN = new Colour(0, 255, 0);
    public static final Colour RED = new Colour(255, 0, 0);

    private byte red, green, blue;

    public Colour(int red, int green, int blue) {
        this.red = intToByte(red);
        this.green = intToByte(green);
        this.blue = intToByte(blue);
    }

    private static byte intToByte(int number) {
        if (MIN_HEX_VALUE <= number && number <= MAX_HEX_VALUE) {
            return (byte) number;
        } else {
            throw new IllegalArgumentException("Colour takes values between "
                                               + MIN_HEX_VALUE + " and "
                                               + MAX_HEX_VALUE + ".");
        }
    }

    private String byteToHex(byte value) {
        return String.format("%02X", value);
    }

    public String toString() {
        return String.format("#%s%s%s", byteToHex(red), byteToHex(green),
                             byteToHex(blue));
    }
}
