package editor.util;

public class Position implements Cloneable {
    private double x, y;

    public Position(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    @Override
    public Position clone() throws CloneNotSupportedException {
        Position clone;
        try {
            clone = (Position) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException("Could not clone Position", e);
        }
        return clone;
    }
}
